from flask import Flask, render_template, make_response
import json
import random
import os

app = Flask(__name__)
root = os.path.dirname(__file__)

AIRPORT_DATA = os.path.join(root, "data", "airports.json") # TODO add json file
AIRCRAFT_DATA = os.path.join(root, "data", "aircrafts.json")

aircraft_data = json.load(open(AIRCRAFT_DATA, "rt", encoding="UTF-8"))
#airport_data = json.load(open(AIRPORT_DATA, "rt", encoding="UTF-8"))
# Sample airports data
airport_data = [
    { "path": "https://upload.wikimedia.org/wikipedia/commons/b/b2/Los_Angeles_International_Airport_Aerial_Photo.jpg",
    "city": "Los Angeles, United States", "aptName": "Los Angeles International Airport"},
    { "path": "https://upload.wikimedia.org/wikipedia/commons/0/03/Howard_Beach-JFK_Airport.jpg",
    "city": "New York, United States", "aptName": "John F. Kennedy International Airport"},
    { "path": "https://upload.wikimedia.org/wikipedia/commons/b/b8/Aerial_View_of_Frankfurt_Airport_1.jpg",
    "city": "Frankfurt, Germany", "aptName": "Frankfurt Airport"}
    ]

def get_random_list_from(old_list, size):
    """

    :param old_list: list to get elements from
    :param size: size of new list
    :return: new list of random elements from old_list
    """
    return [random.choice(old_list) for i in range(size)]

@app.route("/rest/airports/<int:num>")
def get_airports(num):
    """
    Enables cross-origin request to get random list of airports

    :param num: Number of airports
    :return: JSON list of airport objects
    """
    data = json.dumps(get_random_list_from(airport_data, num))
    response = make_response(data)
    response.headers['Access-Control-Allow-Origin'] = ['*']
    return response

@app.route("/rest/aircrafts/<int:num>")
def get_aircrafts(num):
    """
    Enables cross-origin request to get random list of aircrafts

    :param num: Number of aircrafts
    :return: JSON list of aircraft objects
    """
    data = json.dumps(get_random_list_from(aircraft_data, num))
    response = make_response(data)
    response.headers['Access-Control-Allow-Origin'] = ['*']
    return response

@app.route("/airports/<int:num>")
def show_airports(num):
    """

    :param num: Number of airports in carousel
    :return: HTML page with carousel
    """
    return render_template("airports.html", photos=get_random_list_from(airport_data, num))

@app.route("/aircrafts/<int:num>")
def show_aircrafts(num):
    """

    :param num: Number of aircrafts in carousel
    :return: HTML page with carousel
    """
    return render_template("aircrafts.html", photos=get_random_list_from(aircraft_data, num))

if __name__ == "__main__":
    #app.run(host='10.232.45.237')
    #app.run(host='192.168.0.6')
    app.run() # localhost
