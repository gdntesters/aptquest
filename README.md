# APTQuest #


Application for presenting aircraft and airport photos as a slideshow where photo's description appears after few seconds, allowing you to take a guess and eventually learn to recognize all aircrafts and airports!

### How to install and run ###
APTQuest requires Python 3. If you have it, download source code directory or git clone it and type in command prompt:

```
#!python

cd APTQuest  # open application root directory
pip install -r requirements.txt  # install required packages
python aptquest.py  # run application server
```

Then, open your browser and visit http://127.0.0.1:5000/aircrafts/10 (default localhost address and port can be changed in server file) to see slideshow of 10 aircraft photos.

### Routes ###

* /aircrafts/*num* - slideshow with *num* aircrafts in browser
* /airports/*num* - slideshow with *num* airports in browser
* /rest/aircrafts/*num* - JSON list of *num* aircraft objects
* /rest/airports/*num* - JSON list of *num* airport objects