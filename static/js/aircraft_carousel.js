/**
 * Created by PawelBylicki on 2015-10-13.
 */
$(document).ready(function(){
    var IMG_CHANGE_TIMEOUT = 20000;
    var LABEL_SHOW_TIMEOUT = 10000;
    /*var photolist = [
    { path: "https://upload.wikimedia.org/wikipedia/commons/b/b2/Los_Angeles_International_Airport_Aerial_Photo.jpg",
    city: "Los Angeles, United States", aptName: "Los Angeles International Airport"},
    { path: "https://upload.wikimedia.org/wikipedia/commons/0/03/Howard_Beach-JFK_Airport.jpg",
    city: "New York, United States", aptName: "John F. Kennedy International Airport"},
    { path: "https://upload.wikimedia.org/wikipedia/commons/b/b8/Aerial_View_of_Frankfurt_Airport_1.jpg",
    city: "Frankfurt, Germany", aptName: "Frankfurt Airport"}
    ];*/

    photolist.forEach(function(apt){
        $('<div class="item">' +
        '<img class="first-slide" src="' + apt.imageLink + '" alt="First slide"> ' +
        '<div class="container"> <div class="carousel-caption"> <h1>' + apt.aircraftName + '</h1></div> </div> </div>').appendTo(".carousel-inner");
    });

    $('.item').first().addClass('active');
    //$('.carousel-caption').addClass('hidden');
    $('.carousel-caption').fadeOut(50);
    $('#myCarousel').carousel({interval: IMG_CHANGE_TIMEOUT, pause: false});
    setTimeout(function(){
        $('.carousel-caption').fadeIn('slow');
        //$('.carousel-caption').removeClass('hidden');
    }, LABEL_SHOW_TIMEOUT);
    $('#myCarousel').on('slide.bs.carousel', function () {
        //$('.carousel-caption').addClass('hidden');
        $('.carousel-caption').fadeOut('fast');
    });
    $('#myCarousel').on('slid.bs.carousel', function () {
        setTimeout(function(){
            //$('.carousel-caption').removeClass('hidden');
            $('.carousel-caption').fadeIn('slow');
        }, LABEL_SHOW_TIMEOUT);
    });
});